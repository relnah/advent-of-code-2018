package se.relnah.advent.eighteen.day2

import se.relnah.advent.eighteen.ReadInput

fun main(args: Array<String>) {
    val idInput = ReadInput().toList("2", "input.txt")

    var two = 0
    var three = 0

    idInput.forEach { id ->
        var foundTwo = false
        var foundThree = false

        for (c in id) {

            val count = id.chars().filter {
                it == c.toInt()
            }.count().toInt()
            if (!foundTwo && count == 2) {
                two++
                foundTwo = true
            } else if (!foundThree && count == 3) {
                three++
                foundThree = true
            } else if (foundTwo && foundThree) {
                break
            }
        }
    }

    println(two * three)
}