package se.relnah.advent.eighteen.day2

import se.relnah.advent.eighteen.ReadInput
import kotlin.streams.toList

fun main(args: Array<String>) {
    val idList = ReadInput().toList("2", "input.txt")
    val diffList = mutableListOf<String>()

    idList.forEach { idSource ->
        diffList.addAll(idList.parallelStream().filter { idInput ->
            diffByOne(idSource, idInput)
        }.toList())
    }

    val diffPos = diffPosition(diffList[0], diffList[1])
    println(diffList[0].removeRange(diffPos, diffPos + 1))

}

fun diffByOne(source: String, input: String): Boolean {
    if (source == input) {
        return false
    }
    if (source.length != input.length) {
        return false
    }
    var diff = 0

    for ((index, c) in source.withIndex()) {

        if (input[index] != c) {
            diff++
        }

        if (diff > 1) {
            return false
        }
    }

    return diff == 1
}

fun diffPosition(source: String, input: String): Int {
    if (source.length != input.length) {
        return -1
    }
    for ((index, c) in source.withIndex()) {
        if (input[index] != c) {
            return index
        }
    }

    return -1
}