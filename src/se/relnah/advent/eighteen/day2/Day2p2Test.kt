package se.relnah.advent.eighteen.day2

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class Day2p2Test {

    @Test
    fun diffByOneTest() {
        assertTrue(diffByOne("123", "124"))
        assertTrue(diffByOne("133", "123"))

        assertFalse(diffByOne("1234", "124"))
        assertFalse(diffByOne("124", "124"))
        assertFalse(diffByOne("1345", "1246"))
    }

    @Test
    fun diffPositionTest() {
        assertEquals(1, diffPosition("123", "113"))
        assertEquals(-1, diffPosition("123", "123"))
        assertEquals(-1, diffPosition("123", "1234"))
    }
}