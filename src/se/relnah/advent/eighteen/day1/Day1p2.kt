package se.relnah.advent.eighteen.day1

import se.relnah.advent.eighteen.ReadInput
import kotlin.streams.toList

fun main(args: Array<String>) {

    val frequencyChangeStream = ReadInput()
        .toStream("1", "input.txt")
        .mapToDouble(String::toDouble)
        .toList()

    val foundFrequencies = mutableListOf(0.0)
    val duplicateFrequencies = mutableListOf<Double>()

    while (duplicateFrequencies.size < 1) {
        for (freq in frequencyChangeStream) {
            val current = foundFrequencies.last() + freq

            if (foundFrequencies.contains(current)) {
                duplicateFrequencies.add(current)
            }

            foundFrequencies.add(current)
        }
    }
    println(duplicateFrequencies.first())
}