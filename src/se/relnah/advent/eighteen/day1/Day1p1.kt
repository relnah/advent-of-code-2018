package se.relnah.advent.eighteen.day1

import se.relnah.advent.eighteen.ReadInput

fun main(args: Array<String>) {
    val result = ReadInput()
        .toStream("1", "input.txt")
        .mapToDouble(String::toDouble)
        .sum()
    println(message = result)
}
