package se.relnah.advent.eighteen

import java.io.File

class ReadInput {

    fun toStream(day: String, fileName: String) =
            File("src/se/relnah/advent/eighteen/day$day/$fileName")
                .readLines()
                .stream()

    fun toList(day: String, fileName: String) =
            File("src/se/relnah/advent/eighteen/day$day/$fileName")
                .readLines()
}